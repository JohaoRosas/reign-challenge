import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Hit } from './hit';
import { Injectable } from '@angular/core';
import { environment} from '../environments/environment'

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}; 

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor( private http: HttpClient) {
   }

   getHits(): Observable<Hit[]> {
    return this.http.get<Hit[]>(environment.service.hits.getAll)
      .pipe(
        tap(hits => console.log('fetched hits')),
        catchError(this.handleError('getHits', []))
      );
  }

  
  updateStatus(id : string): Observable<Hit> {
    return this.http.put(environment.service.hits.updateStatus + id, {}, httpOptions).pipe(
      tap(_ => console.log(`updated hit id=${id}`)),
      catchError(this.handleError<any>('updateHit'))
    );
  }

   private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
}
