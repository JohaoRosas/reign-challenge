import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Hit } from '../hit';

@Component({
  selector: 'app-hits',
  templateUrl: './hits.component.html',
  styleUrls: ['./hits.component.css']
})
export class HitsComponent implements OnInit {

displayedColumns: string[] = ['title', 'author'];
data: Hit[] = [];
isLoadingResults = true;

  constructor(private api: ApiService) { }

getAll = function ( ) {
  this.api.getHits()
  .subscribe((res: any) => {
    this.data = res;
    console.log(this.data);
    this.isLoadingResults = false;
  }, err => {
    console.log(err);
    this.isLoadingResults = false;
  });
}

  ngOnInit(): void {
    this.getAll();
  }

  updateStatus = function (params:String) {
    this.api.updateStatus(params)
    .subscribe((res: any) => {
      this.data = res;
      console.log(this.data);
      this.getAll();
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  formatAMPM= function (dt) { 

    var date = new Date(dt);
    let today = new Date();
    let today_ = new Date();
    let yesterday = new Date(today_.setDate(today.getDate() - 1));
    console.log(date)
    console.log(today)
    console.log(yesterday)


    if (date.getFullYear()==today.getFullYear() && date.getMonth()==today.getMonth() && date.getDate() == today.getDate()) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      let minutes_ = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes_ + ' ' + ampm;
      return strTime;
    } 
 
    else if( date.getFullYear()==yesterday.getFullYear() && date.getMonth()==yesterday.getMonth() && date.getDate() == yesterday.getDate() ){
      return 'Yesterday';
    }
    else{
      let monthNames =["Jan","Feb","Mar","Apr",
      "May","Jun","Jul","Aug",
      "Sep", "Oct","Nov","Dec"];

      let day = date.getDate();

      let monthIndex = date.getMonth();
      let monthName = monthNames[monthIndex];

      let year = date.getFullYear();

      return `${monthName} ${day}`;  
    }
   
  }

}
