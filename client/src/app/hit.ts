export class Hit{
     created_at: Date;
     title: string;
     url: string;
     author: string;
     points: string;
     story_text: string;
     comment_text: string;
     num_comments: string;
     story_id : number;
     story_title    : string;
     story_url: string;
     parent_id: number;
     created_at_i: number;
     _tags :string[];
     objectID : string;
     _highlightResult : _highlightResult;
}

export class _highlightResult {
     author: author;
     comment_text: comment_text;
     story_title: story_title;
     story_url: story_url; 
}

export class author {
     value: string;
     matchLevel: string;
     matchedWords: string[];
}

export class comment_text {
     value: string;
     matchLevel: string;
     matchedWords: string[];
}


export class story_title {
     value: string;
     matchLevel: string;
     matchedWords: string[];
}

export class story_url {
     value: string;
     matchLevel: string;
     matchedWords: string[];
}