import { NgModule } from '@angular/core';
import { Routes,RouterModule  } from '@angular/router';
import { HitsComponent } from './hits/hits.component';

const routes: Routes = [
    {
      path: 'hits',
      component: HitsComponent,
      data: { title: 'List of hits' }
    }, 
    { path: '',
    redirectTo: '/hits',
    pathMatch: 'full'
  }
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }