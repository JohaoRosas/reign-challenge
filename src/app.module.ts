import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HitsModule } from '../hits/hits.module';
import { ScheduleModule } from '@nestjs/schedule';
import {TasksModule } from '../task/task.module'

@Module({
  imports: [
    HitsModule,
    ScheduleModule.forRoot(),
    TasksModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
