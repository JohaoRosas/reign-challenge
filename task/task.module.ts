import { Module,HttpModule } from '@nestjs/common';
import { TaskService } from './task.service';

@Module({
  providers: [TaskService],
  imports: [HttpModule],
})
export class TasksModule {}