import { Injectable, Logger ,HttpService} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule'; 

@Injectable()
export class TaskService {

  constructor( 
    private readonly httpService: HttpService
    ) {}

  private readonly logger = new Logger(TaskService.name);

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() { 
    this.logger.debug('Called every hour'); 
    console.log(new Date()) 
    const response = await this.httpService.get('http://localhost:3000/api/hits').toPromise();    
  }

  
}