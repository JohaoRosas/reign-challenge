import { Inject, Injectable,HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { HitDto } from './dto/hit.dto';
import { Hit } from './interfaces/hit.interface'; 

@Injectable()
export class HitService {

  constructor(
  @Inject('HIT_MODEL') private readonly hitModel: Model<Hit>) {}

  async create(hitDto: HitDto): Promise<Hit> {
    const createdHit = new this.hitModel(hitDto);
    return await createdHit.save();
  }

  async findWithFilter(query : any, sort : any ): Promise<Hit[]> {
    return await this.hitModel.find(query).sort(sort).exec();
  }

  async findAll(): Promise<Hit[]> {
    return await this.hitModel.find().exec();
  }

  async find(id: string): Promise<Hit[]> {
    return await this.hitModel.find()/* ById(id) */.exec();
  }

  async update(id: string, hitDto: HitDto): Promise<Hit> {
    return await this.hitModel.findByIdAndUpdate(id, hitDto);
  }

  async delete(id: string, hitDto: HitDto): Promise<Hit> {
    return await this.hitModel.findByIdAndRemove(id);
  }
}
