import { Connection } from 'mongoose';
import { HitSchema } from './schemas/hit.schemas';

export const hitProviders = [
    {
        provide: 'HIT_MODEL',
        useFactory: (connection: Connection) => connection.model('Hit', HitSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
