import * as mongoose from 'mongoose';

export const HitSchema = new mongoose.Schema({
    created_at: { type: Date,  required: true  },
    title: { type: String, required: false },
    url: { type: String, required: false },
    author: { type: String, required: true },
    points: { type: String, required: false },
    story_text: { type: String, required: false },
    comment_text: { type: String, required: false },
    num_comments: { type: String, required: false },
    story_id: { type: Number, required: false },
    story_title: { type: String, required: false },  
    story_url: { type: String, required: false },
    parent_id: { type: Number, required: false }, 
    created_at_i: { type: Number, required: true }, 
    _tags: [String],
    objectID: { type: Number, required: true }, 
    _highlightResult : {
        author : {
            value: { type: String, required: true }, 
            matchLevel: { type: String, required: true }, 
            matchedWords: [String], 
        },
        comment_text : {
            value: { type: String, required: false }, 
            matchLevel: { type: String, required: false }, 
            matchedWords: [String], 
        },
        story_title : {
            value: { type: String, required: false }, 
            matchLevel: { type: String, required: false }, 
            matchedWords: [String], 
        },
        story_url : {
            value: { type: String, required: false }, 
            matchLevel: { type: String, required: false }, 
            matchedWords: [String], 
        },
    } ,
    status: { type: Boolean, default:true },
});
