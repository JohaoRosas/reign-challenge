import { Document } from 'mongoose';

export class Hit extends Document {
    readonly created_at: Date;
    readonly title: string;
    readonly url: string;
    readonly author: string;
    readonly points: string;
    readonly story_text: string;
    readonly comment_text: string;
    readonly num_comments: string;
    readonly story_id : number;
    readonly story_title    : string;
    readonly story_url: string;
    readonly parent_id: number;
    readonly created_at_i: number;
    readonly _tags :string[];
    readonly objectID : string;
    readonly _highlightResult : _highlightResult;
    status : boolean;
}

export class _highlightResult {
    readonly author: author;
    readonly comment_text: comment_text;
    readonly story_title: story_title;
    readonly story_url: story_url; 
}

export class author {
    readonly value: string;
    readonly matchLevel: string;
    readonly matchedWords: string[];
}

export class comment_text {
    readonly value: string;
    readonly matchLevel: string;
    readonly matchedWords: string[];
}


export class story_title {
    readonly value: string;
    readonly matchLevel: string;
    readonly matchedWords: string[];
}

export class story_url {
    readonly value: string;
    readonly matchLevel: string;
    readonly matchedWords: string[];
}