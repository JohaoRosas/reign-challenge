import { Module,HttpModule } from '@nestjs/common';
import { HitsController } from './hits.controller'; 
import { HitService } from './hit.service';
import { hitProviders } from './hit.provider';
import { DatabaseModule } from '../database/database.module';

@Module({ 
    imports: [HttpModule,DatabaseModule],
    controllers: [HitsController], 
    providers: [HitService, ...hitProviders],
})
export class HitsModule {}
