import { Body, Controller, Get,HttpService, Param, Put } from '@nestjs/common'; 
import { HitDto } from './dto/hit.dto';
import { HitService } from './hit.service';
import { Hit } from './interfaces/hit.interface';

@Controller('hits')
export class HitsController {
    constructor(private readonly httpService: HttpService,
        private readonly hitService: HitService,
        ) {
            this.proccess();
        }


    @Get()
    async proccess(): Promise<Hit[]> {
        const response = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise();   
        if(response && response.data && response.data.hits && response.data.hits.length>0 ){
            
             let hits = response.data.hits;
             hits.forEach(async element => {
                let hit_model : HitDto = element;
                let query = {objectID : hit_model.objectID};

              let filter =await   this.hitService.findWithFilter(query,{});
                if (filter.length==0) {
                    this.hitService.create(hit_model);
                }
                
             });
         }
        return [];
    }

     @Get("findAll")
    async findAll(): Promise<Hit[]> {
  
        return await  this.hitService.findWithFilter({status : true},{ created_at_i : -1});
                
         }
           
         @Put(':id')
         async update(@Param('id') id: string ) {

            let query = {objectID : parseInt(id)};

            let hitResponse =await   this.hitService.findWithFilter(query,{});

            console.log("hitResponse")
            console.log(hitResponse)
            console.log("hitResponse")

            if (hitResponse.length>0 ){
                hitResponse[0].status= false;
                return this.hitService.update(hitResponse[0]._id, hitResponse[0]);
            }

            return null;
     }
 
}
